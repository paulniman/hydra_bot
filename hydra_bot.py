#!/usr/bin/env python3
# -*- coding: utf-8 -*-

################################################################################
#
# @Author: Paul Niman
# @Created:   March 23, 2019 - 13:00:32
# @Last Modified: April 01, 2019 - 22:50:10
#
################################################################################



import discord

hydra = discord.Client()

@hydra.event
async def on_ready():
    print(f'Hydra running. Cut off 1 head, 2 more take its place')


@hydra.event
async def on_message(message):
    pass
    if message.author == hydra.user:
        return


@hydra.event
async def on_message_delete(message):
    # Post the message twice, but only if it was just text.
    # Ignore attachments for now
    if not message.attachments:
        print(f'Message was deleted. Reposting it twice.')
        await hydra.send_message(message.channel, content=message.content)
        await hydra.send_message(message.channel, content=message.content)
    else:
        print('Ignoring messages with attachments for now.')


if __name__ == '__main__':
    hydra.run('key here')
